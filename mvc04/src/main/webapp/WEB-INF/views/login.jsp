<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Login</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/static/bootstrap/css/bootstrap.min.css"/>" rel=" stylesheet"/>
    <link href="<c:url value="/static/main.css"/>" rel=" stylesheet"/>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="main-login main-center">

    <c:url var="loginUrl" value="/login"/>
    <form class="form-horizontal" method="post" action="${loginUrl}">
        <div class="input-group input-sm">
            <label for="user" class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></label>
            <input type="text" class="form-control" id="user" name="ssoId" placeholder="user" required/>
        </div>

        <div class="input-group input-sm">
            <label for="pass" class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></label>
            <input type="password" class="form-control" id="pass" name="password" placeholder="password" required/>
        </div>

        <div class="input-group input-sm">
            <button type="submit" class="btn btn-default">Login</button>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/static/bootstrap/js/bootstrap.min.js"/>"></script>

</body>
</html>
