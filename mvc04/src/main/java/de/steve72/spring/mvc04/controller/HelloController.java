package de.steve72.spring.mvc04.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * HelloController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class HelloController {

    // das wäre der öffentliche Teil
    @RequestMapping(value = {"/", "/home", "/index"})
    public String homePage(ModelMap modelMap) {

        // Text für eine Bildschirmmeldung
        modelMap.put("greeting", "Willkommen auf meiner Seite!");

        return "welcome";
    }


    @RequestMapping(value = "/admin")
    public String adminPage(ModelMap modelMap) {

        modelMap.put("user", getUserName());

        return "admin";
    }


    @RequestMapping("/login")
    public String loginPage() {

        return "login";
    }


    @RequestMapping(value = "/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {

        // wir holen uns das aktuell Auth-Objekt aus dem Security-Context
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {

            // wir erzeugen einen neuen Handler, der sich für uns abmeldet
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        // nach Abmeldung auf der Startseite weiter
        return "redirect:/login?logout";
    }


    @RequestMapping(value = "/access_denied")
    public String accessDeniedPage(ModelMap modelMap) {

        modelMap.put("user", getUserName());

        return "access-denied";
    }


    private String getUserName() {

        String result = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {

            // wir casten Object -> UserDetails
            result = ((UserDetails) principal).getUsername();
        } else {
            result = principal.toString();
        }

        return result;
    }
}
