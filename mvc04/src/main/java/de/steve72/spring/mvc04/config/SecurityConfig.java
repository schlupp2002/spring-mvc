package de.steve72.spring.mvc04.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


/**
 * SecurityConfig
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Die Methode definiert "Muster" (matches), die bei
     * allen Anfragen geprüft werden.
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
              .antMatchers("/", "/home", "/index").permitAll()
              .antMatchers("/admin/**").access("hasRole('ADMIN')")
              .and()
                .formLogin()
                    .loginPage("/login")
                        .usernameParameter("ssoId")
                        .passwordParameter("password")
              .and().csrf()
              .and().exceptionHandling().accessDeniedPage("/access_denied");
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication().withUser("steve").password("linux").roles("USER");
        auth.inMemoryAuthentication().withUser("admin").password("linux").roles("ADMIN");
    }
}
