package de.steve72.spring.mvccs.service;

import de.steve72.spring.mvccs.dao.DaoInterface;

import java.util.List;


/**
 * ServiceAbstract
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public abstract class ServiceAbstract<T> implements ServiceInterface<T> {

    protected DaoInterface<T> daoInterface;


    public ServiceAbstract(DaoInterface<T> daoInterface) {

        this.daoInterface = daoInterface;
    }


    @Override
    public T findById(Object id) {

        return daoInterface.findById(id);
    }


    @Override
    public List<T> findAll() {

        return daoInterface.findAll();
    }
}
