package de.steve72.spring.mvccs.service;

import de.steve72.spring.mvccs.dao.DaoInterface;
import de.steve72.spring.mvccs.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * ServiceCity
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service("serviceCity")
@Transactional
public class ServiceCity extends ServiceAbstract<City> {

    @Autowired
    public ServiceCity(@Qualifier("daoCity") DaoInterface<City> genericDao) {

        super(genericDao);
    }
}
