package de.steve72.spring.mvccs.dao;

import java.util.List;


/**
 * DaoInterface
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface DaoInterface<T> {

    T findById(Object Id);

    List<T> findAll();
}
