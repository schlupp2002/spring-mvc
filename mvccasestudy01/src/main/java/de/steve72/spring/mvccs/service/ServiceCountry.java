package de.steve72.spring.mvccs.service;

import de.steve72.spring.mvccs.dao.DaoInterface;
import de.steve72.spring.mvccs.model.City;
import de.steve72.spring.mvccs.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * ServiceCountry
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service("serviceCountry")
@Transactional
public class ServiceCountry extends ServiceAbstract<Country> {

    @Autowired
    public ServiceCountry(@Qualifier("daoCountry") DaoInterface<Country> genericDao) {

        super(genericDao);
    }
}
