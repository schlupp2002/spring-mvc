package de.steve72.spring.mvccs.dao;

import de.steve72.spring.mvccs.model.City;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * DaoCity
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
@Repository("daoCity")
public class DaoCity extends DaoAbstract<City> {

    public DaoCity() {

        super(City.class);
    }
}
