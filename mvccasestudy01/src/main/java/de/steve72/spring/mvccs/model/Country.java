package de.steve72.spring.mvccs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Country
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long countryId;

    private String countryName;


    public Long getCountryId() {

        return countryId;
    }


    public void setCountryId(Long countryId) {

        this.countryId = countryId;
    }


    public String getCountryName() {

        return countryName;
    }


    public void setCountryName(String countryName) {

        this.countryName = countryName;
    }
}
