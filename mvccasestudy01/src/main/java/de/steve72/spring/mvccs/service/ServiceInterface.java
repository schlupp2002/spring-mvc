package de.steve72.spring.mvccs.service;

import java.util.List;


/**
 * ServiceInterface
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface ServiceInterface<T> {

    T findById(Object id);

    List<T> findAll();
}
