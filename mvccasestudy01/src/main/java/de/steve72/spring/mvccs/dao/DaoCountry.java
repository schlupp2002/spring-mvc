package de.steve72.spring.mvccs.dao;

import de.steve72.spring.mvccs.model.Country;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * DaoCountry
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("daoCountry")
public class DaoCountry extends DaoAbstract<Country> {

    public DaoCountry() {

        super(Country.class);
    }
}
