package de.steve72.spring.mvccs.controller;

import de.steve72.spring.mvccs.config.Pages;
import de.steve72.spring.mvccs.model.City;
import de.steve72.spring.mvccs.service.ServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * MainController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class MainController {

    @Autowired
    private ServiceInterface<City> serviceCity;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {

        City city = serviceCity.findById(1L);

        modelMap.put("city", city);

        return Pages.INDEX.getPage();
    }
}
