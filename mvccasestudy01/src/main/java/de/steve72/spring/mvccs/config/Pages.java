package de.steve72.spring.mvccs.config;

/**
 * Pages
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public enum Pages {

    INDEX("index");

    private String page;


    Pages(String page) {

        this.page = page;
    }


    public String getPage() {

        return page;
    }
}
