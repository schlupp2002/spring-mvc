package de.steve72.spring.mvccs.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * DaoAbstract
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public abstract class DaoAbstract<T> implements DaoInterface<T> {

    private Class<T> clazz;

    @Autowired
    private SessionFactory sessionFactory;


    public DaoAbstract(Class<T> clazz) {

        this.clazz = clazz;
    }


    @Override
    public T findById(Object id) {

        return (T) getCurrentSession()
              .createCriteria(clazz)
              .add(Restrictions.idEq(id))
              .uniqueResult();
    }


    @Override
    public List<T> findAll() {

        return getCurrentSession()
              .createCriteria(clazz)
              .list();
    }


    private Session getCurrentSession() {

        Session session = null;

        try {

            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {

            session = sessionFactory.openSession();
        }

        return session;
    }
}
