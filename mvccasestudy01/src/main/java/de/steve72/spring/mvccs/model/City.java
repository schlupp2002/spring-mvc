package de.steve72.spring.mvccs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * City
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cityId;

    private String cityName;


    public Long getCityId() {

        return cityId;
    }


    public void setCityId(Long cityId) {

        this.cityId = cityId;
    }


    public String getCityName() {

        return cityName;
    }


    public void setCityName(String cityName) {

        this.cityName = cityName;
    }
}
