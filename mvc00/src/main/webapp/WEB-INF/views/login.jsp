<%--
  Created by IntelliJ IDEA.
  User: bauer
  Date: 25.08.16
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MVC 00 Formular</title>
</head>
<body>
<h2>Wir testen die HTTP-POST-Methode</h2>

<p style="color: red">${errMsg}</p>

<form action="" method="post">

    <label for="user">User:</label>
    <br/>
    <input type="text" id="user" name="user"/>

    <br/>
    <br/>

    <label for="pass">Pass:</label>
    <br/>
    <input type="password" id="pass" name="pass"/>

    <br/>
    <br/>
    <input type="submit" id="submit" name="submit" value="ok"/>


</form>


</body>
</html>
