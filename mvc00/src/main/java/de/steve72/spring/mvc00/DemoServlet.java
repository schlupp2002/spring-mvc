package de.steve72.spring.mvc00;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * DemoServlet
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@WebServlet(urlPatterns = "/demo.do")
public class DemoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // ein Writer, mit wir in das Response-Objekt schreiben können
        PrintWriter out = resp.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>MVC00</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h2>Willkommen bei unserem ersten Servlet</h2>");
        out.println("</body>");
        out.println("</html>");
    }
}
