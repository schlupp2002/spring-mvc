package de.steve72.spring.mvc00;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * DemoServletJSP
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
@WebServlet(urlPatterns = "/demojsp.do")
public class DemoServletJSP extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // wir wollen diesmal als Antwort auf die GET-Anfrage eine JSP-Seite ausliefern
        req.getRequestDispatcher("/WEB-INF/views/demoservletjsp.jsp").forward(req, resp);
    }
}
