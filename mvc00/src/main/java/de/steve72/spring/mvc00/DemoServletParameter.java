package de.steve72.spring.mvc00;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * DemoServletParameter
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
@WebServlet(urlPatterns = "/demopara.do")
public class DemoServletParameter extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // wir holen uns die Parameter aus dem Request-Objekt
        String name = req.getParameter("name");

        // ..und stellen diese als Attribute zur Verfügung
        req.setAttribute("name", name);
        req.setAttribute("pass", req.getParameter("pass"));


        req.getRequestDispatcher("/WEB-INF/views/demopara.jsp").forward(req,resp);
    }
}
