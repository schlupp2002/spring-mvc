package de.steve72.spring.mvc00;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Login
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
@WebServlet(urlPatterns = "/login.do")
public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Formular anzeigen lassen
        req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String user = req.getParameter("user");
        String pass = req.getParameter("pass");

        req.setAttribute("user", user);
        req.setAttribute("pass", pass);


        if( user.equals("steve") && pass.equals("linux"))
            req.getRequestDispatcher("/WEB-INF/views/welcome.jsp").forward(req, resp);
        else {

            req.setAttribute("errMsg", "invalid credentials");
            req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);

        }
    }
}
