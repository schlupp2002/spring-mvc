package de.steve72.spring.mvc02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * HomeController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class HomeController {

    @RequestMapping("/")
    public String hello() {

        return "hello";
    }
}
