package de.steve72.spring.mvc03.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;


/**
 * AppInit
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class AppInit implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext context) throws ServletException {

        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        ctx.register(AppConfig.class);
        ctx.setServletContext(context);

        // das Dispatcher-Server registrieren
        ServletRegistration.Dynamic servlet = context.addServlet(
              "dispatcher",
              new DispatcherServlet(ctx)
        );

        // das Servlet automatisch starten
        servlet.setLoadOnStartup(1);

        // das Servlet mappen
        servlet.addMapping("/");

    }
}
