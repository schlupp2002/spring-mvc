package de.steve72.spring.mvc03.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * HelloController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class HelloController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String helloPage(ModelMap modelMap) {

        return "hello";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutPage(ModelMap modelMap) {

        return "about";
    }

    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String shopPage(ModelMap modelMap) {

        return "shop";
    }

}
