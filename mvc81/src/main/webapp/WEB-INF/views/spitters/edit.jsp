<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>

    <title>Create Spitter</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Apache Tiles setzt in der tiles.xml den jeweiligen Seitentitel -->
    <title><tiles:getAsString name="title"/></title>

    <!-- wir lassen die JSTL den Wert für href generieren -->
    <link href="<c:url value="/static/main.css" />" rel="stylesheet"/>

    <!-- Bootstrap -->
    <link href="<c:url value="/static/bootstrap/css/bootstrap.min.css"/>" rel=" stylesheet"/>

    <!-- Font für FormularIcons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div style="width: 80%; margin: 0 auto;">
    <h2>Create a free Spitter account</h2>

    <sf:form method="post" modelAttribute="spitter" cssClass="form-horizontal">

        <c:set var="errorUsername">
            <sf:errors path="username"/>
        </c:set>

        <c:set var="errorPassword">
            <sf:errors path="password"/>
        </c:set>

        <c:set var="errorEmail">
            <sf:errors path="email"/>
        </c:set>

        <c:set var="isSubmitted">
            <%=(request.getMethod().equalsIgnoreCase("post")) ? true : false%>
        </c:set>

        <div class="form-group has-feedback ${ not empty errorUsername? "has-error":""} ${(isSubmitted && empty errorUsername)?"has-success":""}">

            <label class="control-label col-sm-2" for="user_screen_name">username:</label>
            <div class=" col-sm-10">
                <sf:input
                        path="username"
                        placeholder="username"
                        size="15"
                        maxlength="15"
                        id="user_screen_name"
                        cssClass="form-control"
                        aria-describedby="inputUsernameStatus"/>

                <c:if test="${not empty errorUsername}">
                    <span class="glyphicon glyphicon-exclamation-sign form-control-feedback" aria-hidden="true"></span>
                    <span id="inputUsernameStatus" class="sr-only">(error)</span>
                </c:if>
                <c:if test="${isSubmitted && empty errorUsername}">
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputUsernameStatus" class="sr-only">(error)</span>
                </c:if>


                <span class="help-block">Help: Don't use spaces, please!</span>
                <sf:errors path="username" cssClass="error"/>

            </div>
        </div>


        <div class="form-group has-feedback ${ not empty errorPassword? "has-error":""} ${(isSubmitted && empty errorPassword)?"has-success":""}">

            <label class="control-label col-sm-2" for="user_password">password:</label>
            <div class=" col-sm-10">
                <sf:password
                        path="password"
                        placeholder="password"
                        size="30"
                        showPassword="true"
                        id="user_password"
                        cssClass="form-control"
                        aria-describedby="inputPasswordStatus"/>

                <c:if test="${not empty errorPassword}">
                    <span class="glyphicon glyphicon-exclamation-sign form-control-feedback" aria-hidden="true"></span>
                    <span id="inputPasswordStatus" class="sr-only">(error)</span>
                </c:if>
                <c:if test="${isSubmitted && empty errorPassword}">
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputPasswordStatus" class="sr-only">(error)</span>
                </c:if>

                <span class="help-block">Help: at least 6 characters (be tricky)</span>
                <sf:errors path="password" cssClass="error"/>
            </div>
        </div>

        <div class="form-group has-feedback ${ not empty errorEmail? "has-error":""} ${(isSubmitted && empty errorEmail)?"has-success":""}">

            <label class="control-label col-sm-2" for="user_email">Email Address</label>
            <div class=" col-sm-10">
                <sf:input
                        path="email"
                        placeholder="email address"
                        size="30"
                        id="user_email"
                        cssClass="form-control"
                        aria-describedby="inputEmailStatus"/>

                <c:if test="${not empty errorEmail}">
                    <span class="glyphicon glyphicon-exclamation-sign form-control-feedback" aria-hidden="true"></span>
                    <span id="inputEmailStatus" class="sr-only">(error)</span>
                </c:if>
                <c:if test="${isSubmitted && empty errorEmail}">
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputEmailStatus" class="sr-only">(success)</span>
                </c:if>

                <span class="help-block">Help: In case you forgot something</span>
                <sf:errors path="email" cssClass="error"/>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label for="user_send_email_newsletter">
                        <sf:checkbox
                                path="updateByEmail"
                                id="user_send_email_newsletter"/>
                        Send me email updates
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create my account</button>
            </div>
        </div>

    </sf:form>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/static/bootstrap/js/bootstrap.min.js"/>"></script>

</body>
</html>
