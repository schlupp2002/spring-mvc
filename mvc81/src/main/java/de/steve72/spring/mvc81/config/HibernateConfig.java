package de.steve72.spring.mvc81.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;


/**
 * HibernateConfig
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Configuration
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class HibernateConfig {

    @Autowired
    private Environment env;


    @Bean(name = "datasource")
    public DataSource getDataSource() {

        return new JndiDataSourceLookup().getDataSource(env.getProperty("jndi.name"));
    }


    @Bean(name = "sessionfactory")
    public LocalSessionFactoryBean getSessionFactory() {

        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();

        // hier wird die Bean-erzeugende Methode (Zeile 35ff) aufgerufen
        sessionFactoryBean.setDataSource(getDataSource());

        // hier sollen die Model-Klassen gesucht werden
        sessionFactoryBean.setPackagesToScan(new String[]{"de.steve72.spring.mvc81.model"});
        sessionFactoryBean.setHibernateProperties(hibernateProperties());

        return sessionFactoryBean;
    }


    @Bean
    @Autowired
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {

        HibernateTransactionManager txManager = new HibernateTransactionManager();

        txManager.setSessionFactory(sessionFactory);

        return txManager;
    }


    @Bean
    public PersistenceExceptionTranslationPostProcessor getPersistenceExceptionTranslationPostProcessor() {

        return new PersistenceExceptionTranslationPostProcessor();
    }


    private Properties hibernateProperties() {

        Properties props = new Properties();

        props.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        props.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        props.setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));

        return props;
    }
}