package de.steve72.spring.mvc01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * HomeController01
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class HomeController01 {

    @RequestMapping(value="/sayhello01")
    @ResponseBody
    public String sayHello() {

        return "Hello World 65%";
    }
}

/*

    1) durch die Annotation @Controller wird unserer Klasse zu einem möglichen Handler
        für das Dispatcher-Servlet

    2) die Methode sayHello() würde getriggert, wenn /sayhello01 in der URL angefordert wird

    3) standardmäßig ist die Zeichenkette als Rückgabewert einer Controller-Methode
        immer der Name der View

       für den Fall, dass die Zeichenkette Bestandteil der Antwort sein soll,
       muss die Methode zusätzlich mit @ResponseBody annotiert
 */
