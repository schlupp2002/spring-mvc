package de.steve72.spring.mvc01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * LoginController01
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class LoginController01 {

    @RequestMapping("/login01")
    public String showLoginForm() {

        return "login";
    }
}
