package de.steve72.spring.mvc01.controller;

import de.steve72.spring.mvc01.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * LoginController02
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class LoginController02 {

    @Autowired
    LoginService loginService;


    @RequestMapping(value = "/login02", method = RequestMethod.GET)
    public String showLoginForm() {

        return "login";
    }


    @RequestMapping(value = "/login02", method = RequestMethod.POST)
    public String evalForm(@RequestParam String user, @RequestParam String pass, ModelMap model) {

        String page = "welcome";

        if (!loginService.isValid(user, pass)) {

            // nichts ist gut
            model.put("msg", "invalid credentials");

            page = "login";
        }

        return page;
    }
}

/*

(1)     dieser LoginController soll das leere Formular anzeigen lassen (GET) oder
        die übermittelten Post-Parameter an das Formular weitergeben (POST)

(2)     die Parameter des Formulars können als @RequestParam-annotierte Methodenparameter
        gelesen werden

(3)     Daten vom Controller in Richtung View lassen sich über eine ModelMap übertragen

 */






