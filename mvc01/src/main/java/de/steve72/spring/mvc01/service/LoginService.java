package de.steve72.spring.mvc01.service;

import org.springframework.stereotype.Service;


/**
 * LoginService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
public class LoginService {

    public boolean isValid(String user, String pass) {

        return user.equals("steve") && pass.equals("linux");
    }
}
