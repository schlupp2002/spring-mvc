package de.steve72.spring.mvc01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * HomeController02
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class HomeController02 {

    @RequestMapping("/sayhello02")
    public String sayHello() {

        // die Zeichenkette soll diesmal der logische Name der View sein
        return "hello";
    }


    @RequestMapping("/helloagain")
    public String helloAgain() {

        return "again";
    }
}

/*

(1)     die return-Zeichnekette soll gemäß dem Spring MVC der Name der View sein

(2)     damit der logische View-Name auf eine Datei abgebildet werden kann, bedarf
        es eines View-Resolvers

            "hello"   ---> View-Resolver ---> /WEB-INF/views/hello.jsp

                                                Präfix: /WEB-INF/views/
                                                Suffix: .jsp

 */
