<%--
  Created by IntelliJ IDEA.
  User: bauer
  Date: 29.08.16
  Time: 13:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MVC 01</title>
</head>
<body>
<h2>Bitte Anmeldedaten eingeben</h2>

<p style="color: red">${msg}</p>
<form method="post">

    <label for="user">Benutzer:</label>
    <br/>
    <input type="text" id="user" name="user"/>
    <br/>
    <br/>

    <label for="pass">Passwort:</label>
    <br/>
    <input type="password" id="pass" name="pass" />
    <br/>
    <br/>
    <input type="submit" id="submit" name="submit" value="ok"/>

</form>

</body>
</html>
