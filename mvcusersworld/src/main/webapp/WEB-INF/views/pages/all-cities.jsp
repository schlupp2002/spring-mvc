<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>City</h1><small>complete rowset of ${cities.size()} cities</small>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Einwohner</th>
    </tr>
    </thead>

    <tbody>

    <c:forEach var="city" items="${cities}">
        <tr>
            <td>${city.id}</td>
            <td>${city.name}</td>
            <td>${city.countrycode.name}</td>
            <td>${city.district}</td>
            <td>${city.population}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
