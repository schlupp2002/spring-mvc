<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="main-login main-center">

    <c:url var="loginUrl" value="/login"/>
    <form class="form-horizontal" method="post" action="${loginUrl}">
        <div class="input-group input-sm">
            <label for="user" class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></label>
            <input type="text" class="form-control" id="user" name="user" placeholder="user" required/>
        </div>

        <div class="input-group input-sm">
            <label for="pass" class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></label>
            <input type="password" class="form-control" id="pass" name="pass" placeholder="password" required/>
        </div>

        <div class="input-group input-sm">
            <button type="submit" class="btn btn-default">Login</button>
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>