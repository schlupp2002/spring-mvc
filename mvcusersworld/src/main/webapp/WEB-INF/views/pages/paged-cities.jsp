<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>Cities
        <small>paged</small>
    </h1>
</div>

<nav aria-label="Page navigation">
    <ul class="pagination">
        <li>
            <a href="${pageContext.request.contextPath}/city/page/1" aria-label="First">
                <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
            </a>
        </li>

        <c:forEach var="page" items="${paginator}">
            <li>
                <a href="${pageContext.request.contextPath}/city/page/${page}">${page}</a>
            </li>
        </c:forEach>

        <li>
            <a href="${pageContext.request.contextPath}/city/page/${lastPage}" aria-label="Last">
                <span class="glyphicon glyphicon-fast-forward"  aria-hidden="true"></span>
            </a>
        </li>
    </ul>
</nav>

<a href="${pageContext.request.contextPath}/city/new"><span class="glyphicon glyphicon-plus-sign"></span> neue Stadt hinzufügen</a>

<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Einwohner</th>
        <th>Aktion</th>
    </tr>
    </thead>

    <tbody>

    <c:forEach var="city" items="${cities}">
        <tr>
            <td>${city.id}</td>
            <td>${city.name}</td>
            <td>${city.countrycode.name}</td>
            <td>${city.district}</td>
            <td>${city.population}</td>
            <td>
                <a href="${pageContext.request.contextPath}/city/detail/${city.id}">
                    <span class="glyphicon glyphicon-zoom-in"></span></a>

                <a href="${pageContext.request.contextPath}/city/edit/${city.id}">
                    <span class="glyphicon glyphicon-pencil marginXL"></span></a>

                <a href="${pageContext.request.contextPath}/city/delete/${city.id}">
                    <span class="glyphicon glyphicon-trash marginXL"></span></a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>