package de.steve72.spring.mvcusersworld.service;

import de.steve72.spring.mvcusersworld.dao.BasicDao;
import de.steve72.spring.mvcusersworld.model.CountryLanguage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * CountryLanguageServiceImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
@Transactional
public class CountryLanguageServiceImpl implements CountryLanguageService {

    @Autowired
    private BasicDao<CountryLanguage> countryLanguageDao;


    @Override
    public Long getCount() {

        return countryLanguageDao.getCount();
    }


    @Override
    public CountryLanguage findByID(Object primaryKey) {

        return countryLanguageDao.getById(primaryKey);
    }


    @Override
    public List<CountryLanguage> findAll() {

        return countryLanguageDao.getAll();
    }


    @Override
    public List<CountryLanguage> findAllWithLimits(Integer offset, Integer count) {

        return countryLanguageDao.getAllWithLimits(offset, count);
    }


    @Override
    public void insert(CountryLanguage entity) {

        countryLanguageDao.create(entity);
    }


    @Override
    public void update(CountryLanguage entity) {

        countryLanguageDao.update(entity);
    }
}