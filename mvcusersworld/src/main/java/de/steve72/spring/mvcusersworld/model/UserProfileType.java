package de.steve72.spring.mvcusersworld.model;

import java.io.Serializable;


/**
 * UserProfileType
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public enum UserProfileType implements Serializable {

    USER("USER"), DBA("DBA"), ADMIN("ADMIN");

    String userProfileType;


    UserProfileType(String userProfileType) {

        this.userProfileType = userProfileType;
    }


    public String getUserProfileType() {

        return userProfileType;
    }
}
