package de.steve72.spring.mvcusersworld.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;


/**
 * City
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
public class Country {

    public enum Continent {

        Asia, Europe, North_America, Africa, Oceania, Antarctica, South_America;
    }

    @Id
    private String code;

    // default: FetchType.LAZY
    @OneToMany(targetEntity = City.class, mappedBy = "countrycode", fetch = FetchType.LAZY)
    private Set<City> cities;

    // default: FetchType.LAZY
    @OneToMany(targetEntity = CountryLanguage.class, mappedBy = "countrycode", fetch = FetchType.LAZY)
    private Set<CountryLanguage> countryLanguages;

    private String name;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum('Asia', 'Europe', 'North_America', 'Africa', 'Oceania', 'Antarctica', 'South_America')")
    private Continent continent;

    private String region;

    private Double surfacearea;

    private Integer indepyear;

    private Long population;

    private Double lifeexpectancy;

    private Double gnp;

    private Double gnpold;

    private String localname;

    private String governmentform;

    private String headofstate;

    private Integer capital;

    private String code2;


    public Country() {

    }


    @Override
    public String toString() {

        return String.format("[%s] (%s, %s, %s, %s, %.2f, %d, %d, %.2f, %.2f, %.2f, %s, %s, %s, %d, %s)",
              this.getClass().getSimpleName(),
              code,
              name,
              continent,
              region,
              surfacearea,
              indepyear,
              population,
              lifeexpectancy,
              gnp,
              gnpold,
              localname,
              governmentform,
              headofstate,
              capital,
              code2
        );
    }


    //region Getter und Setter
    public String getCode() {

        return code;
    }


    public void setCode(String code) {

        this.code = code;
    }


    public Set<City> getCities() {

        return cities;
    }


    public void setCities(Set<City> cities) {

        this.cities = cities;
    }


    public String getName() {

        return name;
    }


    public void setName(String name) {

        this.name = name;
    }


    public Continent getContinent() {

        return continent;
    }


    public void setContinent(Continent continent) {

        this.continent = continent;
    }


    public String getRegion() {

        return region;
    }


    public void setRegion(String region) {

        this.region = region;
    }


    public Double getSurfacearea() {

        return surfacearea;
    }


    public void setSurfacearea(Double surfacearea) {

        this.surfacearea = surfacearea;
    }


    public Integer getIndepyear() {

        return indepyear;
    }


    public void setIndepyear(Integer indepyear) {

        this.indepyear = indepyear;
    }


    public Long getPopulation() {

        return population;
    }


    public void setPopulation(Long population) {

        this.population = population;
    }


    public Double getLifeexpectancy() {

        return lifeexpectancy;
    }


    public void setLifeexpectancy(Double lifeexpectancy) {

        this.lifeexpectancy = lifeexpectancy;
    }


    public Double getGnp() {

        return gnp;
    }


    public void setGnp(Double gnp) {

        this.gnp = gnp;
    }


    public Double getGnpold() {

        return gnpold;
    }


    public void setGnpold(Double gnpold) {

        this.gnpold = gnpold;
    }


    public String getLocalname() {

        return localname;
    }


    public void setLocalname(String localname) {

        this.localname = localname;
    }


    public String getGovernmentform() {

        return governmentform;
    }


    public void setGovernmentform(String governmentform) {

        this.governmentform = governmentform;
    }


    public String getHeadofstate() {

        return headofstate;
    }


    public void setHeadofstate(String headofstate) {

        this.headofstate = headofstate;
    }


    public Integer getCapital() {

        return capital;
    }


    public void setCapital(Integer capital) {

        this.capital = capital;
    }


    public String getCode2() {

        return code2;
    }


    public void setCode2(String code2) {

        this.code2 = code2;
    }
    //endregion


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        return code != null ? code.equals(country.code) : country.code == null;

    }


    @Override
    public int hashCode() {

        return code != null ? code.hashCode() : 0;
    }
}
