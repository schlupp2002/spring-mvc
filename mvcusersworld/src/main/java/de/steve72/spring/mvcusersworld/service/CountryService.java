package de.steve72.spring.mvcusersworld.service;

import de.steve72.spring.mvcusersworld.model.Country;

import java.util.List;

/**
 * CountryService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface CountryService extends BasicService<Country> {

    Country findByID(Object primaryKey);

    List<Country> findAll();

    List<Country> findAllWithLimits(Integer offset, Integer count);

    void insert(Country entity);

    void update(Country entity);
}
