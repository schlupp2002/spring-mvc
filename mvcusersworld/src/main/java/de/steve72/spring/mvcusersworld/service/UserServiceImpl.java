package de.steve72.spring.mvcusersworld.service;

import de.steve72.spring.mvcusersworld.dao.BasicDao;
import de.steve72.spring.mvcusersworld.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * UserServiceImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private BasicDao<User> userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public void update(User entity) {

        entity.setPassword(passwordEncoder.encode(entity.getPassword()));

        userDao.update(entity);
    }


    @Override
    public User findByRestriction(String restriction, Object value) {

        return userDao.getByRestriction(restriction, value);
    }
}