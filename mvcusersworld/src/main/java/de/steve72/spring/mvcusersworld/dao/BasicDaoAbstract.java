package de.steve72.spring.mvcusersworld.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * BasicDaoAbstract
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public abstract class BasicDaoAbstract<T> implements BasicDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    protected Class<T> clazz;
    protected String orderCrit;


    public void setClazz(Class<T> clazz) {

        this.clazz = clazz;
    }


    public void setOrderCrit(String orderCrit) {

        this.orderCrit = orderCrit;
    }


    @Override
    public Long getCount() {

        return (Long) getCurrentSession()
              .createCriteria(clazz)
              .setProjection(Projections.rowCount())
              .uniqueResult();
    }


    @Override
    public T getById(Object id) {

        return (T) getCurrentSession()
              .createCriteria(clazz)
              .add(Restrictions.idEq(id))
              .uniqueResult();
    }


    @Override
    public List<T> getAll() {

        return getCurrentSession()
              .createCriteria(clazz)
              .addOrder(Order.asc(orderCrit))
              .list();
    }


    @Override
    public T getByRestriction(String restriction, Object value) {

        return (T) getCurrentSession()
              .createCriteria(clazz)
              .add(Restrictions.eq(restriction, value))
              .uniqueResult();
    }


    @Override
    public List<T> getAllWithLimits(Integer offset, Integer count) {

        return getCurrentSession()
              .createCriteria(clazz)
              .setFirstResult(offset)
              .setMaxResults(count)
              .addOrder(Order.asc(orderCrit))
              .list();
    }


    @Override
    public void create(T entity) {

        getCurrentSession().saveOrUpdate(entity);
    }


    @Override
    public void update(T entity) {

        getCurrentSession().merge(entity);
    }


    @Override
    public void delete(T entity) {

        getCurrentSession().delete(entity);
    }


    protected final Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }
}
