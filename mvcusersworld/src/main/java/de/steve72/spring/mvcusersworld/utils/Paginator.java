package de.steve72.spring.mvcusersworld.utils;

import de.steve72.spring.mvcusersworld.service.BasicService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Paginator
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class Paginator<T> {

    @Autowired
    private BasicService<T> cityService;

    private Integer lastPage;
    private Integer maxResults = 10;
    private Integer activePage;

    private final Integer pageCount = 11;
    private final Integer defaultPosi = pageCount / 2 + 1;


    /**
     * Der Konstruktor bekommt den Service injiziert und berechnet über die
     * Anzahl der Datensätze in der DB-Tabelle und die Anzahl der darzustellenden
     * Datensätze auf dem Bildschirm die letzte Bildschirmseitennummer
     *
     * @param service
     */
    public Paginator(BasicService<T> service) {

        this.cityService = service;
        lastPage = service.getCount().intValue() / maxResults + 1;
    }


    /**
     * Ändert die Anzahl der maximal darzustellenden Bildschirmdatensätze und
     * berechnet auf dieser Basis die Nummer der letzten Bildschirmseite neu.
     *
     * @param maxResults
     */
    public void setMaxResults(Integer maxResults) {

        this.maxResults = maxResults;
        lastPage = cityService.getCount().intValue() / maxResults + 1;
    }


    /**
     * Liefert die letztmögliche Bildschirmseite für das über den Service erreichbare DAO.
     *
     * @return Nummer der letzten Bildschirmseite
     */
    public Integer getLastPage() {

        return lastPage;
    }


    public Integer getActivePage() {

        return activePage;
    }


    public Integer[] getButtons(Integer page) {

        this.activePage = page;

        // das Feld enthält ein Array mit 11 Seitennummern
        Integer[] result = new Integer[pageCount];
        Integer offset = 0;

        // die Seitennummer der aktuellen Seite soll an zentraler Position im Paginator stehen
        if (page >= defaultPosi)
            offset = defaultPosi;
        else
            offset = page;

        if (lastPage - page <= pageCount - defaultPosi)
            offset = pageCount - lastPage + page;

        // berechne die Bildschirmseitennummern für eine vorgebene Bildschirmseite
        // Bsp.: page = 100, dann 97,98,99,[100],101,102...
        for (int i = 0; i < pageCount; i++) {

            //if (page - offset <= lastPage)
                result[i] = page - offset + 1;
            //else
            //    result[i] = 0;
            offset--;
        }

        return result;
    }
}
