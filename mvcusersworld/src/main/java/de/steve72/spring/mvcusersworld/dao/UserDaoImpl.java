package de.steve72.spring.mvcusersworld.dao;

import de.steve72.spring.mvcusersworld.model.User;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


/**
 * UserDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("userDao")
@Transactional
public class UserDaoImpl extends BasicDaoAbstract<User> {

    public UserDaoImpl() {

        setClazz(User.class);
    }
}
