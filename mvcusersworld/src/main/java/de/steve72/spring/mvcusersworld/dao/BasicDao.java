package de.steve72.spring.mvcusersworld.dao;

import java.util.List;


/**
 * BasicDao
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public interface BasicDao<T> {

    // liefert die Anzahl der Datensätze einer Tabelle
    Long getCount();

    // liefert ein Datenbankobjekt anhand des Schlüsselwerts
    T getById(Object id);

    // liefert alle Datenbankobjekte
    List<T> getAll();

    // liefert einen Datensatz passend zu einer UniqueRestriction
    T getByRestriction(String restriction, Object value);

    // liefert eine Page
    List<T> getAllWithLimits(Integer offset, Integer count);

    // fügt ein neues Datenbankobjekt hinzu
    void create(T entity);

    // aktualisiert ein ausgewähltes Datenbankobjekt
    void update(T entity);

    // löscht eine ein ausgewähltes Datenbankobjekt
    void delete(T entity);

}
