package de.steve72.spring.mvcusersworld.dao;

import de.steve72.spring.mvcusersworld.model.CountryLanguage;
import org.springframework.stereotype.Repository;


/**
 * CountryLanguageDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("countryLanguageDao")
public class CountryLanguageDaoImpl extends BasicDaoAbstract<CountryLanguage> {

    public CountryLanguageDaoImpl() {

        setClazz(CountryLanguage.class);
        setOrderCrit("countrycode");
    }
}
