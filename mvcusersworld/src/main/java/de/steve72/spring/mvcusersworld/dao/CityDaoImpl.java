package de.steve72.spring.mvcusersworld.dao;

import de.steve72.spring.mvcusersworld.model.City;
import org.springframework.stereotype.Repository;


/**
 * CityDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("cityDao")
public class CityDaoImpl extends BasicDaoAbstract<City> {

    public CityDaoImpl() {

        setClazz(City.class);
        setOrderCrit("id");
    }
}