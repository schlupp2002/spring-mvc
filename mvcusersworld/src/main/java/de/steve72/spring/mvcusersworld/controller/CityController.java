package de.steve72.spring.mvcusersworld.controller;

import de.steve72.spring.mvcusersworld.model.City;
import de.steve72.spring.mvcusersworld.model.Country;
import de.steve72.spring.mvcusersworld.service.BasicService;
import de.steve72.spring.mvcusersworld.service.CityService;
import de.steve72.spring.mvcusersworld.service.CountryService;
import de.steve72.spring.mvcusersworld.utils.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


/**
 * CityController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    @Autowired
    private CountryService countryService;

    private Integer maxResult = 10;


    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public String getAll(ModelMap modelMap) {

        List<City> cities = cityService.findAll();

        modelMap.put("cities", cities);

        return "all-cities";
    }


    // /city/page
    @RequestMapping(value = {"/page/{page}"})
    public String getCitiesWithLimit(@PathVariable String page, ModelMap modelMap) {

        // Offset aus der Pfadvariable ermitteln
        Integer start = 0;

        try {
            start = Integer.valueOf(page) - 1;
            start = start >= 0 ? start : 0;
        } catch (NumberFormatException ex) {

            // im Fehlerfall bleiben wir beim Offset 0
            start = 0;
        }

        // einen Paginator bereitstellen
        Paginator<City> paginator = new Paginator<>(cityService);
        paginator.setMaxResults(maxResult);
        modelMap.put("paginator", paginator.getButtons(start + 1));
        modelMap.put("lastPage", paginator.getLastPage());
        modelMap.put("activePage", paginator.getActivePage());

        // die Page aus der Datenbank holen
        List<City> cities = cityService.findAllWithLimits(start * maxResult, maxResult);

        // ..und ins Model packen
        modelMap.put("cities", cities);

        return "paged-cities";
    }


    @RequestMapping("/detail/{id}")
    public String getDetails(@PathVariable Integer id, ModelMap modelMap) {

        modelMap.put("city", cityService.findByID(id));

        return "detail-city";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id, ModelMap modelMap) {

        cityService.delete(cityService.findByID(id));

        return "redirect:/city/page/1";
    }

    @RequestMapping(value = {"/edit", "/new"}, method = RequestMethod.GET)
    public String showEditForm(ModelMap modelMap) {

        modelMap.put("countries", countryService.findAll());

        return "edit-city";
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String evalEditForm(@RequestParam String name,
                               @RequestParam String district,
                               @RequestParam String countrycode,
                               @RequestParam String population,
                               ModelMap modelMap) {

        City city = new City();
        city.setName(name);
        city.setDistrict(district);
        city.setCountrycode(countryService.findByID(countrycode));

        int pop = 0;

        try {
            pop = Integer.valueOf(population);
        } catch (NumberFormatException ex) {

        }

        city.setPopulation(pop);

        cityService.insert(city);

        modelMap.put("city", city);

        return "detail-city";
    }
}
