package de.steve72.spring.mvcusersworld.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


/**
 * CountryLanguagePK
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Embeddable
public class CountryLanguagePK implements Serializable {

    @Column(length = 3)
    private String countryCode;

    @Column(length = 52)
    private String language;


    public CountryLanguagePK() {

    }


    public CountryLanguagePK(String countryCode, String language) {

        this.countryCode = countryCode;
        this.language = language;
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryLanguagePK that = (CountryLanguagePK) o;

        if (!countryCode.equals(that.countryCode)) return false;
        return language.equals(that.language);

    }


    @Override
    public int hashCode() {

        int result = countryCode.hashCode();
        result = 31 * result + language.hashCode();
        return result;
    }


    public String getCountryCode() {

        return countryCode;
    }


    public void setCountryCode(String countryCode) {

        this.countryCode = countryCode;
    }


    public String getLanguage() {

        return language;
    }


    public void setLanguage(String language) {

        this.language = language;
    }


    @Override
    public String toString() {

        return String.format("%s, %s",
              getCountryCode(),
              getLanguage()
        );
    }
}
