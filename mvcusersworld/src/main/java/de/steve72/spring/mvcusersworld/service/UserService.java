package de.steve72.spring.mvcusersworld.service;

import de.steve72.spring.mvcusersworld.model.User;


/**
 * UserService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface UserService {

    void update(User entity);

    User findByRestriction(String restriction, Object value);

}
