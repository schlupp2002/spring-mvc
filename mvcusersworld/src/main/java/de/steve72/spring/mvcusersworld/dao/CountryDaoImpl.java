package de.steve72.spring.mvcusersworld.dao;

import de.steve72.spring.mvcusersworld.model.Country;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * CountryDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("countryDao")
public class CountryDaoImpl extends BasicDaoAbstract<Country> {

    public CountryDaoImpl() {

        setClazz(Country.class);
        setOrderCrit("code");
    }


    public List getCodeAndName() {

        return getCurrentSession()
              .createCriteria(clazz)
              .setProjection(Projections.projectionList()
                    .add(Projections.property("code"))
                    .add(Projections.property("name")))
              .addOrder(Order.asc("code"))
              .list();
    }
}