package de.steve72.spring.mvcusersworld.service;

/**
 * BasicService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface BasicService<T> {

    Long getCount();
}
