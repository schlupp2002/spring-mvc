package de.steve72.spring.mvcusersworld.controller;

import de.steve72.spring.mvcusersworld.model.City;
import de.steve72.spring.mvcusersworld.model.Country;
import de.steve72.spring.mvcusersworld.model.CountryLanguage;
import de.steve72.spring.mvcusersworld.service.BasicService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * HelloController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class PageController {

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {

        modelMap.put("seite", "index");
        modelMap.put("user", getPrincipal());

        return "index";
    }


    @RequestMapping(value = {"/shop"}, method = RequestMethod.GET)
    public String shopPage(ModelMap modelMap) {

        modelMap.put("seite", "shop");

        return "shop";
    }

    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap modelMap) {

        modelMap.put("user", getPrincipal());

        return "access-denied";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {

        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {

            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }

        return "index";
    }

    private String getPrincipal() {

        String userName = "";

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = "";
        }

        return userName;
    }
}
