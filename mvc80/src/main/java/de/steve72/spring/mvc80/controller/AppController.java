package de.steve72.spring.mvc80.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * AppController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class AppController {

}
