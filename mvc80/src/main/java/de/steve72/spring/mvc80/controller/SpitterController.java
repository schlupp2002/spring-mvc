package de.steve72.spring.mvc80.controller;

import de.steve72.spring.mvc80.model.Spitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


/**
 * SpitterController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
@RequestMapping("/spitters")
public class SpitterController {

    // Methode ist zuständig für  /spitters?new
    @RequestMapping(method = RequestMethod.GET, params = "new")
    public String createSpitterProfile(Model model) {

        model.addAttribute(new Spitter());

        return "spitters/edit";
    }


    @RequestMapping(method = RequestMethod.POST)
    public String addSpitterFromForm(@Valid Spitter spitter, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {

            return "spitters/edit";
        }

        // @Todo: eine Aktion, falls Spitterobjekt o.B. war
        model.addAttribute(spitter);

        return "redirect:/spitters/" + spitter.getUsername();
    }









    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public String showSpitterProfile(@PathVariable String username, Model model) {

        model.addAttribute(username);

        return "spitters/view";
    }
}
