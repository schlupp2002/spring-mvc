package de.steve72.spring.mvc80.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;


/**
 * Spitter
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
public class Spitter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 3, max = 20, message = "Error: username must be between 3 and 20 characters long")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Error: username must be alphanumeric with no spaces")
    private String username;

    @Size(min = 6, max = 20, message = "Error: between 6 and 20 characters are required")
    private String password;

    private String fullname;

    @NotEmpty(message = "Error: Email address is required")
    @Email(message = "Error: malformed email address")
    private String email;

    private boolean updateByEmail;


    public Long getId() {

        return id;
    }


    public void setId(Long id) {

        this.id = id;
    }


    public String getUsername() {

        return username;
    }


    public void setUsername(String username) {

        this.username = username;
    }


    public String getPassword() {

        return password;
    }


    public void setPassword(String password) {

        this.password = password;
    }


    public String getFullname() {

        return fullname;
    }


    public void setFullname(String fullname) {

        this.fullname = fullname;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail(String email) {

        this.email = email;
    }


    public boolean isUpdateByEmail() {

        return updateByEmail;
    }


    public void setUpdateByEmail(boolean updateByEmail) {

        this.updateByEmail = updateByEmail;
    }
}
