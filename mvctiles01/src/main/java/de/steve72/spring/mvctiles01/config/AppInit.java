package de.steve72.spring.mvctiles01.config;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;


/**
 * AppInit
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

public class AppInit extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {

        return new Class[]{AppConfig.class};
    }


    @Override
    protected Class<?>[] getServletConfigClasses() {

        return null;
    }


    @Override
    protected String[] getServletMappings() {

        return new String[]{"/"};
    }
}
