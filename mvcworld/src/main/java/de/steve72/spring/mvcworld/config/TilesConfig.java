package de.steve72.spring.mvcworld.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;


/**
 * TilesConfig
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Configuration
public class TilesConfig {

    @Bean
    public TilesConfigurer tilesConfigurer() {

        TilesConfigurer tilesConfigurer = new TilesConfigurer();

        // das Array signalisiert, dass es eine oder mehrere Definitionsdateien seien dürfen
        tilesConfigurer.setDefinitions(new String[]{"/WEB-INF/views/**/tiles.xml"});

        // default is false
        tilesConfigurer.setCheckRefresh(true);

        return tilesConfigurer;
    }
}
