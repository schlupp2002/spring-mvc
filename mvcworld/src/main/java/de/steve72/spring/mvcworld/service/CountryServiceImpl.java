package de.steve72.spring.mvcworld.service;

import de.steve72.spring.mvcworld.dao.BasicDao;
import de.steve72.spring.mvcworld.dao.CountryDaoImpl;
import de.steve72.spring.mvcworld.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * CountryServiceImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    @Autowired
    private BasicDao<Country> countryDao;


    @Override
    public Long getCount() {

        return countryDao.getCount();
    }


    @Override
    public Country findByID(Object primaryKey) {

        return countryDao.getById(primaryKey);
    }


    @Override
    public List<Country> findAll() {

        return countryDao.getAll();
    }


    @Override
    public List<Country> findAllWithLimits(Integer offset, Integer count) {

        return countryDao.getAllWithLimits(offset, count);
    }


    @Override
    public void insert(Country entity) {

        countryDao.create(entity);
    }


    @Override
    public void update(Country entity) {

        countryDao.update(entity);
    }
}
