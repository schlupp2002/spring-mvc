package de.steve72.spring.mvcworld.controller;

import de.steve72.spring.mvcworld.model.City;
import de.steve72.spring.mvcworld.service.CityService;
import de.steve72.spring.mvcworld.service.CountryLanguageService;
import de.steve72.spring.mvcworld.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * HelloController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class PageController {

    @Autowired
    CityService cityService;

    @Autowired
    CountryService countryService;

    @Autowired
    CountryLanguageService countryLanguageService;


    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String indexPage(ModelMap modelMap) {

        modelMap.put("seite", "index");

        return "index";
    }





    @RequestMapping(value = {"/shop"}, method = RequestMethod.GET)
    public String shopPage(ModelMap modelMap) {

        modelMap.put("seite", "shop");

        return "shop";
    }
}
