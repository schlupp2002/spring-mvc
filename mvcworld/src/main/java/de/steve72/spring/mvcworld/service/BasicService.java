package de.steve72.spring.mvcworld.service;

import de.steve72.spring.mvcworld.model.City;

import java.util.List;


/**
 * BasicService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface BasicService<T> {

    Long getCount();
}
