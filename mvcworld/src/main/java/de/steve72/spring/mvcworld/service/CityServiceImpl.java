package de.steve72.spring.mvcworld.service;

import de.steve72.spring.mvcworld.dao.BasicDao;
import de.steve72.spring.mvcworld.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * CityServiceImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private BasicDao<City> cityDao;


    @Override
    public Long getCount() {

        return cityDao.getCount();
    }


    @Override
    public City findByID(Object primaryKey) {

        return cityDao.getById(primaryKey);
    }


    @Override
    public List<City> findAll() {

        return cityDao.getAll();
    }


    @Override
    public List<City> findAllWithLimits(Integer offset, Integer count) {

        return cityDao.getAllWithLimits(offset, count);
    }


    @Override
    public void insert(City entity) {

        cityDao.create(entity);
    }


    @Override
    public void update(City entity) {

        cityDao.update(entity);
    }
}