package de.steve72.spring.mvcworld.controller;

import de.steve72.spring.mvcworld.model.Country;
import de.steve72.spring.mvcworld.service.CountryService;
import de.steve72.spring.mvcworld.utils.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * CountryController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    private Integer maxResult = 10;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public String getAll(ModelMap modelMap) {

        modelMap.put("seite", "country");

        List<Country> countries = countryService.findAll();

        modelMap.put("countries", countries);

        return "all-countries";
    }

    @RequestMapping(value = {"/page/{page}"})
    public String getCitiesWithLimit(@PathVariable String page, ModelMap modelMap) {

        // Offset aus der Pfadvariable ermitteln
        Integer start = 0;

        try {
            start = Integer.valueOf(page) - 1;
            start = start >= 0 ? start : 0;
        } catch (NumberFormatException ex) {

            // im Fehlerfall bleiben wir beim Offset 0
            start = 0;
        }

        // einen Paginator bereitstellen
        Paginator<Country> paginator = new Paginator<>(countryService);
        paginator.setMaxResults(maxResult);
        modelMap.put("paginator", paginator.getButtons(start + 1));
        modelMap.put("lastPage", paginator.getLastPage());

        // die Page aus der Datenbank holen
        List<Country> cities = countryService.findAllWithLimits(start * maxResult, maxResult);

        // ..und ins Model packen
        modelMap.put("cities", cities);

        return "paged-cities";
    }
}
