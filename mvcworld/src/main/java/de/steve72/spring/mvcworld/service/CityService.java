package de.steve72.spring.mvcworld.service;

import de.steve72.spring.mvcworld.model.City;

import java.util.List;


/**
 * CityService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface CityService extends BasicService<City> {

    City findByID(Object primaryKey);

    List<City> findAll();

    List<City> findAllWithLimits(Integer offset, Integer count);

    void insert(City entity);

    void update(City entity);
}
