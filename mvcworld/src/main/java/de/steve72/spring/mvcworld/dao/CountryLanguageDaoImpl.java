package de.steve72.spring.mvcworld.dao;

import de.steve72.spring.mvcworld.model.CountryLanguage;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * CountryLanguageDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("countryLanguageDao")
public class CountryLanguageDaoImpl extends BasicDaoAbstract<CountryLanguage> {

    public CountryLanguageDaoImpl() {

        setClazz(CountryLanguage.class);
        setOrderCrit("countrycode");
    }
}
