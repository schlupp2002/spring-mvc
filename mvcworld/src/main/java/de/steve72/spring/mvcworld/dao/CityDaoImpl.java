package de.steve72.spring.mvcworld.dao;

import de.steve72.spring.mvcworld.model.City;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * CityDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("cityDao")
public class CityDaoImpl extends BasicDaoAbstract<City> {

    public CityDaoImpl() {

        setClazz(City.class);
        setOrderCrit("id");
    }
}