package de.steve72.spring.mvcworld.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class CountryLanguage {

    public enum TrueFalse {

        T, F;
    }

    @EmbeddedId
    private CountryLanguagePK countryLanguagePK;

    @Column(columnDefinition = "float(4,1)")
    private Float percentage;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum('T','F')")
    private TrueFalse isOfficial;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countrycode", insertable = false, updatable = false)
    private Country countrycode;


    public CountryLanguage() {

    }


    public CountryLanguagePK getCountryLanguagePK() {

        return countryLanguagePK;
    }


    public void setCountryLanguagePK(CountryLanguagePK countryLanguagePK) {

        this.countryLanguagePK = countryLanguagePK;
    }


    public Float getPercentage() {

        return percentage;
    }


    public void setPercentage(Float percentage) {

        this.percentage = percentage;
    }


    public TrueFalse getIsOfficial() {

        return isOfficial;
    }


    public void setIsOfficial(TrueFalse isOfficial) {

        this.isOfficial = isOfficial;
    }


    public Country getCountrycode() {

        return countrycode;
    }


    public void setCountrycode(Country countrycode) {

        this.countrycode = countrycode;
    }


    @Override
    public String toString() {

        return String.format("[%s] %s, %,4.1f, %s",
              getClass().getSimpleName(),
              getCountryLanguagePK(),
              getPercentage(),
              getIsOfficial());
    }
}
