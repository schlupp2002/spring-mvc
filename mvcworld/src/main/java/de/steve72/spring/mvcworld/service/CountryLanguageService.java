package de.steve72.spring.mvcworld.service;

import de.steve72.spring.mvcworld.model.Country;
import de.steve72.spring.mvcworld.model.CountryLanguage;

import java.util.List;


/**
 * CountryLanguageService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface CountryLanguageService extends BasicService<CountryLanguage> {

    CountryLanguage findByID(Object primaryKey);

    List<CountryLanguage> findAll();

    List<CountryLanguage> findAllWithLimits(Integer offset, Integer count);

    void insert(CountryLanguage entity);

    void update(CountryLanguage entity);
}