package de.steve72.spring.mvcworld.controller;

import de.steve72.spring.mvcworld.model.Country;
import de.steve72.spring.mvcworld.model.CountryLanguage;
import de.steve72.spring.mvcworld.service.CountryLanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * CountryLanguageController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class CountryLanguageController {

    @Autowired
    private CountryLanguageService countryLanguageService;

    @RequestMapping(value = {"/countrylanguage"}, method = RequestMethod.GET)
    public String getAll(ModelMap modelMap) {

        modelMap.put("seite", "country");

        List<CountryLanguage> countryLanguages = countryLanguageService.findAll();

        modelMap.put("countrylanguages", countryLanguages);

        return "all-countrylanguages";
    }
}
