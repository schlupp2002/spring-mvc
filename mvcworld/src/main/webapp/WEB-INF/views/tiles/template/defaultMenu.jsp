<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">
            <img src="${pageContext.request.contextPath}/static/theme01/pics/path3050.png"/>
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

            <li>
                <a href="${pageContext.request.contextPath}/">Home <span class="sr-only">(current)</span></a>
            </li>



            <li>
                <a href="${pageContext.request.contextPath}/shop">Shop</a>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tables
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/city/all">All Cities</a></li>
                    <li><a href="${pageContext.request.contextPath}/country/all">All Countries</a></li>
                    <li><a href="${pageContext.request.contextPath}/countrylanguage">All CountryLanguages</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="${pageContext.request.contextPath}/city/page/1">Cities paged</a></li>
                    <li><a href="#">Countries paged</a></li>
                    <li><a href="#">CountryLanguages paged</a></li>
                </ul>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">

            <li>
                <a href="${pageContext.request.contextPath}/">About</a>
            </li>

            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Suche</button>
            </form>


        </ul>
    </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->