<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Apache Tiles setzt in der tiles.xml den jeweiligen Seitentitel -->
    <title><tiles:getAsString name="title"/></title>

    <!-- Bootstrap -->
    <link href="<c:url value="/static/bootstrap/css/bootstrap.min.css"/>" rel=" stylesheet"/>

    <!-- wir lassen die JSTL den Wert für href generieren -->
    <link href="<c:url value="/static/theme01/css/main.css" />" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div class="jumbotron">
        <div class="container">
            <tiles:insertAttribute name="header"/>
        </div>
    </div>

    <nav class="navbar navbar-default">
        <tiles:insertAttribute name="menu"/>
    </nav>

    <div class="content-wrapper">
        <tiles:insertAttribute name="body"/>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/static/bootstrap/js/bootstrap.min.js"/>"></script>

</body>
</html>
