<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>Citiy
        <small>detailed</small>
    </h1>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">${city.name}
            <a href="${pageContext.request.contextPath}/city/page/1"><span class="glyphicon glyphicon-arrow-up"></span></a></h3>
    </div>
    <div class="panel-body">
        <div>${city.countrycode.name}</div>
        <div>${city.district}</div>
        <div>${city.population}</div>
    </div>
</div>