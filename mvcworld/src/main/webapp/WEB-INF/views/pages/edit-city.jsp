<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>City
        <small>edit</small>
    </h1>
</div>

${params}

<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/city/edit">

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Stadt</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Stadt">
        </div>
    </div>


    <div class="form-group">
        <label for="countrycode" class="col-sm-2 control-label">Land</label>
        <div class="col-sm-10">
            <select id="countrycode" name="countrycode" class="form-control">
                <c:forEach var="cc" items="${countries}">
                <option value="${cc.code}">${cc.name}</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="district" class="col-sm-2 control-label">Bundesland</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="district" name="district" placeholder="Bundesland">
        </div>
    </div>

    <div class="form-group">
        <label for="population" class="col-sm-2 control-label">Einwohnerzahl</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="population" name="population" placeholder="Einwohnerzahl">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Eintragen</button>
        </div>
    </div>
</form>