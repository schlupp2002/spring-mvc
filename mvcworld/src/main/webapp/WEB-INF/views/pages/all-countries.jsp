<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>Country</h1><small>complete rowset of ${country.size()} countries</small>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Land</th>
        <th>Bundesland</th>
        <th>Einwohner</th>
    </tr>
    </thead>

    <tbody>

    <c:forEach var="country" items="${countries}">
        <tr>
            <td>${country.id}</td>
            <td>${country.name}</td>
            <td>${country.countrycode.name}</td>
            <td>${country.district}</td>
            <td>${country.population}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>