-- MySQL dump 10.13  Distrib 5.5.50, for debian-linux-gnu (x86_64)
--
-- Host: 10.10.102.81    Database: db_springsecurity
-- ------------------------------------------------------
-- Server version	5.5.50-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Current Database: `db_springsecurity`
--

CREATE DATABASE /*!32312 IF NOT EXISTS */ `db_springsecurity` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_springsecurity`;

--
-- Table structure for table `appUser`
--

DROP TABLE IF EXISTS `appUser`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appUser` (
    `id`        INT(11)      NOT NULL AUTO_INCREMENT,
    `email`     VARCHAR(255) NOT NULL,
    `firstName` VARCHAR(255) NOT NULL,
    `lastName`  VARCHAR(255) NOT NULL,
    `password`  VARCHAR(255) NOT NULL,
    `ssoId`     VARCHAR(255) NOT NULL,
    `state`     VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_kpxiyeu5rbkbbk3a07a0n3wss` (`ssoId`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 6
    DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appUser`
--

LOCK TABLES `appUser` WRITE;
/*!40000 ALTER TABLE `appUser`
    DISABLE KEYS */;
INSERT INTO `appUser`
VALUES (1, 'bill@xyz.com', 'Bill', 'Watcher', 'linux', 'bill', 'Active'), (2, 'danny@xyz.com', 'Danny', 'Theys', 'linux', 'danny', 'Active'),
    (3, 'samy@xyz.com', 'Sam', 'Smith', 'linux', 'sam', 'Active'), (4, 'nicloe@xyz.com', 'Nicole', 'warner', 'linux', 'nicole', 'Active'),
    (5, 'kenny@xyz.com', 'Kenny', 'Roger', 'linux', 'kenny', 'Active');
/*!40000 ALTER TABLE `appUser`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appUser_to_userProfile`
--

DROP TABLE IF EXISTS `appUser_to_userProfile`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appUser_to_userProfile` (
    `userId`        INT(11) NOT NULL,
    `userProfileId` INT(11) NOT NULL,
    PRIMARY KEY (`userId`, `userProfileId`),
    KEY `FKexuy6n3q5yjcevbk6v1nttwmu` (`userProfileId`),
    CONSTRAINT `FKqgjnr9jgivgb95nj2bhjhh6ci` FOREIGN KEY (`userId`) REFERENCES `appUser` (`id`),
    CONSTRAINT `FKexuy6n3q5yjcevbk6v1nttwmu` FOREIGN KEY (`userProfileId`) REFERENCES `userProfile` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appUser_to_userProfile`
--

LOCK TABLES `appUser_to_userProfile` WRITE;
/*!40000 ALTER TABLE `appUser_to_userProfile`
    DISABLE KEYS */;
INSERT INTO `appUser_to_userProfile` VALUES (1, 1), (2, 1), (3, 2), (5, 2), (4, 3), (5, 3);
/*!40000 ALTER TABLE `appUser_to_userProfile`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userProfile`
--

DROP TABLE IF EXISTS `userProfile`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userProfile` (
    `id`   INT(11)     NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_sqjvwv5utbpp45qs9mutsuoc7` (`type`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 4
    DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userProfile`
--

LOCK TABLES `userProfile` WRITE;
/*!40000 ALTER TABLE `userProfile`
    DISABLE KEYS */;
INSERT INTO `userProfile` VALUES (2, 'ADMIN'), (3, 'DBA'), (1, 'USER');
/*!40000 ALTER TABLE `userProfile`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2016-09-13  8:23:17
