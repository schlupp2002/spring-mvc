package de.steve72.spring.mvc05.service;

import de.steve72.spring.mvc05.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * UserServiceImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
@Transactional
public class UserServiceImpl extends BasicServiceAbstract<User> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void persist(User entity) {

        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        super.persist(entity);
    }
}
