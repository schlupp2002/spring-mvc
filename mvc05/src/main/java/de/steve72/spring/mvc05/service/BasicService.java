package de.steve72.spring.mvc05.service;

/**
 * BasicService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface BasicService<T> {

    T findById(Object id);

    T findByRestriction(String field, Object value);

    void persist(T entity);

    void delete(T entity);
}
