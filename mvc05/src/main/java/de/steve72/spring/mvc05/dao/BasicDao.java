package de.steve72.spring.mvc05.dao;

/**
 * BasicDao
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public interface BasicDao<T> {

    T getById(Object id);

    T getByRestriction(String field, Object value);

    void persist(T entity);

    void delete(T entity);
}
