package de.steve72.spring.mvc05.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * User
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
@Table(name = "appUser")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(unique = true, nullable = false)
    private String ssoId;


    @Column(nullable = false)
    private String password;


    @Column(nullable = false)
    private String firstName;


    @Column(nullable = false)
    private String lastName;


    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String state = State.ACTIVE.getState();


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="appUser_to_userProfile",
        joinColumns = { @JoinColumn(name="userId")},
        inverseJoinColumns = { @JoinColumn(name="userProfileId")})
    private Set<UserProfile> userProfiles = new HashSet<>();


    public Integer getId() {

        return id;
    }


    public void setId(Integer id) {

        this.id = id;
    }


    public String getSsoId() {

        return ssoId;
    }


    public void setSsoId(String ssoId) {

        this.ssoId = ssoId;
    }


    public String getPassword() {

        return password;
    }


    public void setPassword(String password) {

        this.password = password;
    }


    public String getFirstName() {

        return firstName;
    }


    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }


    public String getLastName() {

        return lastName;
    }


    public void setLastName(String lastName) {

        this.lastName = lastName;
    }


    public String getEmail() {

        return email;
    }


    public void setEmail(String email) {

        this.email = email;
    }


    public String getState() {

        return state;
    }


    public void setState(String state) {

        this.state = state;
    }


    public Set<UserProfile> getUserProfiles() {

        return userProfiles;
    }


    public void setUserProfiles(Set<UserProfile> userProfiles) {

        this.userProfiles = userProfiles;
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        return ssoId != null ? ssoId.equals(user.ssoId) : user.ssoId == null;

    }


    @Override
    public int hashCode() {

        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ssoId != null ? ssoId.hashCode() : 0);
        return result;
    }
}
