package de.steve72.spring.mvc05.dao;

import de.steve72.spring.mvc05.model.User;
import org.springframework.stereotype.Repository;


/**
 * UserDaoImpl
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Repository("userDao")
public class UserDaoImpl extends BasicDaoAbstract<User> {

    public UserDaoImpl() {

        super(User.class);
    }
}
