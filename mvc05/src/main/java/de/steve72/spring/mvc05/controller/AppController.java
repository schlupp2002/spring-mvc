package de.steve72.spring.mvc05.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * AppController
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Controller
public class AppController {

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homePage(ModelMap modelMap) {

        String user = getPrincipal();
        user = (!user.equals("")) ? user : "guest";

        modelMap.put("user", user );
        modelMap.put("greeting", "hi, welcome to mysite");

        return "welcome";
    }


    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap modelMap) {

        modelMap.put("user", getPrincipal());

        return "admin";
    }


    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap modelMap) {

        modelMap.put("user", getPrincipal());

        return "access-denied";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {

        return "login";
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {

            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }

        return "redirect:/login?logout";
    }


    private String getPrincipal() {

        String userName = "";

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = "";
        }

        return userName;
    }
}
