package de.steve72.spring.mvc05.model;

/**
 * State
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public enum State {

    ACTIVE("Active"), INACTIVE("Inactive"), DELETED("Deleted"), LOCKED("Locked");

    private String state;


    State(String state) {

        this.state = state;
    }


    public String getState() {

        return state;
    }
}
