package de.steve72.spring.mvc05.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;


/**
 * SecurityWebAppInitializer
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer{

    // die Klasse bleibt leer!

    // es ist nur wichtig, dass die Klasse von AbstractSecurityWebApplicationInitializer
    // ableitet, weil dann automatisch von Spring für das DispatcherServlet
    // ein SecurityFilter registriert wird
}
