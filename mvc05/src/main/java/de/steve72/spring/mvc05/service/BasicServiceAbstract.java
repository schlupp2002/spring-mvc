package de.steve72.spring.mvc05.service;

import de.steve72.spring.mvc05.dao.BasicDao;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * BasicServiceAbstract
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public abstract class BasicServiceAbstract<T> implements BasicService<T> {

    @Autowired
    private BasicDao<T> basicDao;


    @Override
    public T findById(Object id) {

        return basicDao.getById(id);
    }


    @Override
    public T findByRestriction(String field, Object value) {

        return basicDao.getByRestriction(field, value);
    }


    @Override
    public void persist(T entity) {

        basicDao.persist(entity);
    }


    @Override
    public void delete(T entity) {

        basicDao.delete(entity);
    }
}
