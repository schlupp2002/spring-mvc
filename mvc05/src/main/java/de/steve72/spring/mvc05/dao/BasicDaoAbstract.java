package de.steve72.spring.mvc05.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * BasicDaoAbstract
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */
public abstract class BasicDaoAbstract<T> implements BasicDao<T> {

    private final Class<T> clazz;

    @Autowired
    private SessionFactory sessionFactory;


    public BasicDaoAbstract(Class<T> clazz) {

        this.clazz = clazz;
    }


    @Override
    public T getById(Object id) {

        return (T) getCurrentSession()
              .createCriteria(clazz)
              .add(Restrictions.idEq(id))
              .uniqueResult();
    }


    @Override
    public T getByRestriction(String field, Object value) {

        return (T) getCurrentSession()
              .createCriteria(clazz)
              .add(Restrictions.eq(field, value))
              .uniqueResult();
    }


    @Override
    public void persist(Object entity) {

        getCurrentSession().saveOrUpdate(entity);
    }


    @Override
    public void delete(Object entity) {

        getCurrentSession().delete(entity);
    }


    private final Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }
}
