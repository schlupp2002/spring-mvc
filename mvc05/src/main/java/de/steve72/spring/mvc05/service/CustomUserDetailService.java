package de.steve72.spring.mvc05.service;

import de.steve72.spring.mvc05.model.User;
import de.steve72.spring.mvc05.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * CustomUserDetailService
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private BasicService<User> userService;


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = userService.findByRestriction("ssoId", s);

        if (user == null) {

            throw new UsernameNotFoundException(String.format("user not found: %s", s));
        }

        return new org.springframework.security.core.userdetails.User(
              user.getSsoId(),
              user.getPassword(),
              user.getState().equals("Active"),
              true,
              true,
              true,
              getGrantedAuthorities(user)
        );
    }


    private List<GrantedAuthority> getGrantedAuthorities(User user) {

        List<GrantedAuthority> authorities = new ArrayList<>();

        for (UserProfile userProfile : user.getUserProfiles()) {

            authorities.add(new SimpleGrantedAuthority("ROLE_" + userProfile.getType()));
        }

        return authorities;
    }
}
