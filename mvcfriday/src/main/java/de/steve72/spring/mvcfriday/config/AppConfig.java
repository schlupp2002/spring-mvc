package de.steve72.spring.mvcfriday.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;


/**
 * AppConfig
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Configuration
@ComponentScan(basePackages = "de.steve72.spring.mvcfriday")
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {

        TilesViewResolver viewResolver = new TilesViewResolver();
        registry.viewResolver(viewResolver);
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // wir mappen das Module-Web-Root auf eine Location
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }
}


