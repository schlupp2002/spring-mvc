package de.steve72.spring.mvcfriday.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * City
 *
 * @author Steffen Bauer <schlupp2002@gmail.com>
 */

@Entity
public class City {

    @Id
    @Column(name = "ID", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countrycode", nullable = false)
    private Country countrycode;

    @Column(name = "District")
    private String district;

    @Column(name = "Population")
    private Integer population;


    public City() {

    }


    //region Getter und Setter
    public Integer getId() {

        return id;
    }


    public void setId(Integer id) {

        this.id = id;
    }


    public String getName() {

        return name;
    }


    public void setName(String name) {

        this.name = name;
    }


    public Country getCountrycode() {

        return countrycode;
    }


    public void setCountrycode(Country countrycode) {

        this.countrycode = countrycode;
    }


    public String getDistrict() {

        return district;
    }


    public void setDistrict(String district) {

        this.district = district;
    }


    public Integer getPopulation() {

        return population;
    }


    public void setPopulation(Integer population) {

        this.population = population;
    }
    //endregion


    @Override
    public String toString() {

        return String.format("[%s] (%d, %s, {%s}, %s, %d)",
              this.getClass().getSimpleName(),
              id,
              name,
              countrycode,
              district,
              population
        );
    }
}
